﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace Strawberry
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = new StreamReader("input.txt");
            int X = int.Parse(reader.ReadLine());
            int Y = int.Parse(reader.ReadLine());
            int Z = int.Parse(reader.ReadLine());
            reader.Close();

            int  result;
            result = (X + Y) - Z;


            if (Z<=result)
            {
                StreamWriter writer = new StreamWriter("output.txt");
                writer.WriteLine($"{result}");
                writer.Close();
            }
            else
            {
                StreamWriter writer = new StreamWriter("output.txt");
                writer.WriteLine($"Impossible");
                writer.Close();
            }



        }
    }
}
